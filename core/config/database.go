package config

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB
var err error

type Resume struct {
	gorm.Model
	ID   uint   `json:"id"`
	Fio  string `json:"fio"`
	url  string `json:"url"`
	City string `json:"city"`
}

type Vacansy struct {
	gorm.Model
	ID      uint   `json:"id"`
	Url     string `json:"url"`
	Company string `json:"company"`
	City    string `json:"city"`
}

type Settings struct {
	gorm.Model
	ID uint `json:"id"`
	// <city>.hh.ru/<vacansy|resume>/<0:9999> или что-бы следить за новыми вакансиями компании, вбиваете полную ссылку на компанию
	Url       string `json:"url_conf"`
	Name_conf string `json:"name_conf"`
}

func Config() {
	db, err := gorm.Open("postgres", "host='127.0.0.1' port=5433 user=rinat dbname=test_go password=930114")

	// db, err := gorm.Open("sqlite3", "prod.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()
	db.AutoMigrate(&Resume{})
	db.AutoMigrate(&Vacansy{})
	db.AutoMigrate(&Settings{})

	db.Create(&Resume{ID: 1, Fio: "Rinat", url: "/vac/2", City: "Tymen"})

	var resume Resume
	var res = db.First(&resume)
	println(res)
}
