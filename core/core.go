package core

import (
	"crypto/sha256"
	"fmt"
	"time"
)

const BlockSize = 64
const Size = 32

type Block struct {
	head      string   // по умолч = None
	token     string   // токен
	befor     []string // Предедуший хэш
	datetime  time.Time
	confirmed bool
}

func (b *Block) head_add() []byte {
	h := sha256.New()
	var hashs string
	if len(b.befor) == 1 {
		// Если есть родитель
		hashs = b.token + b.befor[0] + b.datetime.String()
	} else if len(b.befor) > 1 {
		// Если есть семейное древо
		var hash_befors string
		for _, value := range b.befor {
			hash_befors += value
		}
		hashs = b.token + hash_befors + b.datetime.String()
	} else {
		// Если нет не кого
		hashs = b.token + b.datetime.String()
	}
	h.Write([]byte(hashs))
	bs := h.Sum([]byte{})
	return bs
}

func Password_create() string {
	block := &Block{token: "114exr23", datetime: time.Now(), befor: []string{"sdsa23", "sdsad34sa4f"}}
	var heads string = fmt.Sprintf("%x", block.head_add())
	block.head = heads
	return heads
}
