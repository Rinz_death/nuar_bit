package migrate

import (
	"fmt"

	conf "../core/config"
)

func Migrate() {
	write := fmt.Println
	write("Начало миграции")
	conf.Config()
	write("Миграция закончена успешно")
}
