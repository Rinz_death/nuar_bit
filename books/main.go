package main
import (
	"fmt"
)
type Shape interface {
    calc() float64
}

type Area struct {
	x float64
}

// Метод
func (c *Area) calc() {
	c.x = c.x * 2.2
}

func ark(shapes ...*Shape) float64{
	var area float64
    for _, s := range shapes {
        area += s.calc()
    }
    return area
}

func main(){
	var c = Area{ x: 1.1}
	c.calc()
	fmt.Println(ark(&c))
	fmt.Println(c.x)
}