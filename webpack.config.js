const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = {
  entry: ['./assets/js/main.js', './assets/style/main.scss'],
  output: {
    path: path.resolve(__dirname, 'public/static/'),
    filename: 'js/distr.bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: [{
          loader: 'babel-loader',
          options: { presets: ['es2015'] }
        },
        {
          loader: "raw-loader"
        }]
      },
      {
        test: /\.(sass|scss)$/,
        // loaders: ["css-loader",  "sass-loader"]
        loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader' ])
      }

    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        'window.Tether': 'tether',
    }),
    new ExtractTextPlugin({ // define where to save the file
      filename: 'css/distr.bundle.css',
      allChunks: true,
    }),
  ],
};

module.exports = config;