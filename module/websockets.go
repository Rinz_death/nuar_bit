package main

import (
	"io/ioutil"
	"log"
	"net/url"
	"net/http"
)

var torProxy string = "socks5://127.0.0.1:9050"

func main() {
	torProxyUrl, err := url.Parse(torProxy)
	torTransport := &http.Transport{Proxy: http.ProxyURL(torProxyUrl)}
	client := &http.Client{Transport: torTransport}

	resp, err := client.Get("http://kladr-api.ru/api.php?query=Москва&contentType=city&withParent=1&limit=1")
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	// Read response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("Error reading body of response.", err)
	}
	log.Println(string(body))
	log.Println("Return status code:", resp.StatusCode)
}
